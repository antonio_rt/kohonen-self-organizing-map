close all; clear all;

%%generate n random colors
n_root = 16;
training_set_size = n_root*n_root;

%load fixed training data, instead of generating it

rand_colors = rand(training_set_size, 3); 
load rand_colors2.mat

%generate training set, tweaking colors individually
a=0; b=1.0;
red = a + (b-a).*rand(training_set_size, 1);

a=0; b=0.5;
green = a + (b-a).*rand(training_set_size, 1);

a=0; b=0.5;
blue = a + (b-a).*rand(training_set_size, 1);

rand_colors(:,1) = red;
rand_colors(:,2) = green;
rand_colors(:,3) = blue;

% orig_rand_colors = rand_colors;
% normalized_rand_colors = zeros(size(rand_colors));

reshape_rand_colors = reshape(rand_colors, n_root, n_root, 3);
figure('Name', 'Input Data'), imshow(reshape_rand_colors, 'DisplayRange',[]);


%% generate mxm som grid with 3 features and random weights
grid_size_i = 4;
grid_size_j = 4;

%generate normal luminosity initial color grid
% som_grid = rand (grid_size_i,grid_size_j,3);
% load som_grid4.mat

%generate ray initial som grid
% a=0; b=0.05;
% som_grid = a + (b-a).*rand(grid_size_i, grid_size_j, 1);
% som_grid = repmat(som_grid, [1, 1, 3]);

%generate low luminosity color grid 
a=0; b=0.15;
som_grid = a + (b-a).*rand(grid_size_i, grid_size_j, 3);
load dark_som_grid.mat;

%%generate low red color grid
% a=0; b=0.09;
% red = a + (b-a).*rand(grid_size_i, grid_size_j, 1);
% 
% a=0; b=0.15;
% green = a + (b-a).*rand(grid_size_i, grid_size_j, 1);
% 
% a=0; b=0.15;
% blue = a + (b-a).*rand(grid_size_i, grid_size_j, 1);
% 
% som_grid(:,:,1) = red;
% som_grid(:,:,2) = green;
% som_grid(:,:,3) = blue;
% load low_red_dark_som_grid.mat

figure('Name', 'Original map - Random weight initialization, size 8 x 8'), 
    imshow(som_grid, 'DisplayRange',[]);

%% generate bmu hit map, tracker of attractors

bmu_hit_map = zeros(size(som_grid,1), size(som_grid,2));

%% prompt              
fprintf('Original map shown, Press enter to start training\n');    
pause; 



%% normalize som grid
norm2_som_grid = sqrt(som_grid(:,:,1).^2 + ...
                      som_grid(:,:,2).^2 + ...
                      som_grid(:,:,3).^2);
for i=1 : grid_size_i
    for j=1: grid_size_j
        som_grid(i,j,:) = som_grid(i,j,:) / norm2_som_grid(i,j);
    end
end
                  
%% normalize rand colors before training
norm2_rand_colors = sum(sqrt(rand_colors.^ 2),2);
for i=1: size(rand_colors, 1)
    rand_colors(i,:) = rand_colors(i,:) / norm2_rand_colors(i);
end


%% initial defaults, placed here to save memory
bmu_i    = -1;
bmu_j    = -1;
bmu_dist = 9999;

neigh_i = -1;
neigh_j = -1;

%%

learning_rate = 0.1;
fprintf('\nlearning rate: %f\n', learning_rate);
bmu_hit_neigh_effect = 1.0; %1.0 is no restraint for neighbors
fprintf('effect of bmu hit applied to neighbors: %f\n', bmu_hit_neigh_effect);
fprintf('Training network... \n...')
%% training section 
training_iteration_limit = 100;
tic();
for s=1 : training_iteration_limit
    
    
    %go through training set
    for t_index = 1 : training_set_size
    
           
        %% go through grid, in search of best matching unit
        for grid_index_i = 1 : grid_size_i
            for grid_index_j = 1 : grid_size_j

            dist = ...
                norm(squeeze(rand_colors(t_index,:))...
                   - squeeze(som_grid(grid_index_i, grid_index_j,:))');    
                
            if (dist < bmu_dist)
                    bmu_i = grid_index_i;
                    bmu_j = grid_index_j;
                    bmu_dist = dist;                
            end
                
            end
        end
    
    %% find distance for each feature  
    dist1 = norm(som_grid(bmu_i, bmu_j, 1) - rand_colors(t_index, 1));
    dist2 = norm(som_grid(bmu_i, bmu_j, 2) - rand_colors(t_index, 2));
    dist3 = norm(som_grid(bmu_i, bmu_j, 3) - rand_colors(t_index, 3));
    
    dist1 = som_grid(bmu_i, bmu_j, 1) - rand_colors(t_index, 1);
    dist2 = som_grid(bmu_i, bmu_j, 2) - rand_colors(t_index, 2);
    dist3 = som_grid(bmu_i, bmu_j, 3) - rand_colors(t_index, 3);
 
    %% update bmu
    som_grid(bmu_i, bmu_j, 1) = ...
        som_grid(bmu_i, bmu_j, 1) + learning_rate*(dist1); 
    
    som_grid(bmu_i, bmu_j, 2) = ...
        som_grid(bmu_i, bmu_j, 2) + learning_rate*(dist2); 
    
    som_grid(bmu_i, bmu_j, 3) = ...
       som_grid(bmu_i, bmu_j, 3) + learning_rate*(dist3); 
     
    %% update neighbors
    %update top neighbor
    if (bmu_i ~= 1)
        neigh_i = bmu_i-1;
        neigh_j = bmu_j;
        
        %begin update region
        
        dist1 = som_grid(neigh_i, neigh_j, 1) - rand_colors(t_index, 1);
        dist2 = som_grid(neigh_i, neigh_j, 2) - rand_colors(t_index, 2);
        dist3 = som_grid(neigh_i, neigh_j, 3) - rand_colors(t_index, 3);
        
        som_grid(neigh_i, neigh_j, 1) = ...
          som_grid(neigh_i, neigh_j, 1) + ...
          (learning_rate*(dist1)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 2) = ...
          som_grid(neigh_i, neigh_j, 2) + ...
          (learning_rate*(dist2)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 3) = ...
         som_grid(neigh_i, neigh_j, 3) +...
         (learning_rate*(dist3)*bmu_hit_neigh_effect); 
        
        %end update region
    end
    
    %update botton neighbor
    if (bmu_i ~= grid_size_i)
        neigh_i = bmu_i+1;
        neigh_j = bmu_j;
    
        %begin update region
        
        dist1 = som_grid(neigh_i, neigh_j, 1) - rand_colors(t_index, 1);
        dist2 = som_grid(neigh_i, neigh_j, 2) - rand_colors(t_index, 2);
        dist3 = som_grid(neigh_i, neigh_j, 3) - rand_colors(t_index, 3);
        
        som_grid(neigh_i, neigh_j, 1) = ...
          som_grid(neigh_i, neigh_j, 1) + ...
          (learning_rate*(dist1)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 2) = ...
          som_grid(neigh_i, neigh_j, 2) + ...
          (learning_rate*(dist2)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 3) = ...
         som_grid(neigh_i, neigh_j, 3) +...
         (learning_rate*(dist3)*bmu_hit_neigh_effect); 
        
        %end update region
    
    end
    
    %update left neighbor
    if (bmu_j ~= 1)
        neigh_i  = bmu_i;
        neigh_j  = bmu_j-1;
    
        %begin update region
        
        dist1 = som_grid(neigh_i, neigh_j, 1) - rand_colors(t_index, 1);
        dist2 = som_grid(neigh_i, neigh_j, 2) - rand_colors(t_index, 2);
        dist3 = som_grid(neigh_i, neigh_j, 3) - rand_colors(t_index, 3);
        
        som_grid(neigh_i, neigh_j, 1) = ...
          som_grid(neigh_i, neigh_j, 1) + ...
          (learning_rate*(dist1)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 2) = ...
          som_grid(neigh_i, neigh_j, 2) + ...
          (learning_rate*(dist2)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 3) = ...
         som_grid(neigh_i, neigh_j, 3) +...
         (learning_rate*(dist3)*bmu_hit_neigh_effect); 
        
        %end update region
    
    end
    
    %update right neighbor
    if (bmu_j ~= grid_size_j)
        neigh_i = bmu_i;
        neigh_j = bmu_j+1;
    
       %begin update region
        
        dist1 = som_grid(neigh_i, neigh_j, 1) - rand_colors(t_index, 1);
        dist2 = som_grid(neigh_i, neigh_j, 2) - rand_colors(t_index, 2);
        dist3 = som_grid(neigh_i, neigh_j, 3) - rand_colors(t_index, 3);
        
        som_grid(neigh_i, neigh_j, 1) = ...
          som_grid(neigh_i, neigh_j, 1) + ...
          (learning_rate*(dist1)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 2) = ...
          som_grid(neigh_i, neigh_j, 2) + ...
          (learning_rate*(dist2)*bmu_hit_neigh_effect); 
    
        som_grid(neigh_i, neigh_j, 3) = ...
         som_grid(neigh_i, neigh_j, 3) +...
         (learning_rate*(dist3)*bmu_hit_neigh_effect); 
        
        %end update region
    
    end
    %end updating neighbors
    
    %% update bmu_hit_map
    bmu_hit_map(bmu_i, bmu_j) = bmu_hit_map(bmu_i, bmu_j) +1;
    
    end
    
end
time_to_complete = toc()

max_hits = max(bmu_hit_map(:));

figure('Name', 'BMU Hit Map'),
    imshow(bmu_hit_map, 'Colormap',gray);
    

    

figure('Name', 'Trained Kohonen Map'), 
    imshow(som_grid, 'DisplayRange',[]);
    

